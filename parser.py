import os, sys
import docx
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.layout import LAParams
from pdfminer.converter import PDFPageAggregator


def get_files(path):
    res = []
    for i in os.listdir(path):
        # 去掉临时文件 /Delete temporary files
        if os.path.isfile(path+i) and '~$' not in i and '.DS' not in i:
            # 去重 1.doc 和 1.docx /De-duplicate 1.doc and 1.docx
            if (path+i).split(".")[0] not in str(res):
                res.append(path+i)
    return res


def pdf_reader(file):
    fp = open(file, "rb")
    # 创建一个与文档相关联的解释器 /Create an interpreter associated with the document
    parser = PDFParser(fp)
    # PDF文档对象 /PDF document object
    doc = PDFDocument(parser)
    # 链接解释器和文档对象/Link interpreter and document object
    parser.set_document(doc)
    # doc.set_paeser(parser)
    # 初始化文档 /Initialization document
    # doc.initialize("")
    # 创建PDF资源管理器 /Create PDF Explorer
    resource = PDFResourceManager()
    # 参数分析器 /Parametric analyzer
    laparam = LAParams()
    # 创建一个聚合器 /Create an aggregator
    device = PDFPageAggregator(resource, laparams=laparam)
    # 创建PDF页面解释器 /Create PDF page interpreter
    interpreter = PDFPageInterpreter(resource, device)
    # 使用文档对象得到页面集合 /Use the document object to get the page collection
    res = ''
    for page in PDFPage.create_pages(doc):
        # 使用页面解释器来读取 /Use page interpreter to read
        interpreter.process_page(page)
        # 使用聚合器来获取内容 /Use aggregators to get content
        layout = device.get_result()
        for out in layout:
            if hasattr(out, "get_text"):
                res = res + '' + out.get_text()
    return res


def word_reader(file):
    try:
        # docx 直接读 /docx read directly
        if 'docx' in file:
            res = ''
            f = docx.Document(file)
            for para in f.paragraphs:
                res = res + '\n' +para.text
        else:
            # 先转格式doc>docx /First convert the format doc>docx
            os.system("textutil -convert docx '%s'"%file)
            word_reader(file+'x')
            res = ''
            f = docx.Document(file+'x')
            for para in f.paragraphs:
                res = res + '\n' +para.text
        return res
    except:
        # print(file, 'read failed')
        return ''


def file_reader(file):
    if 'doc' in file:
        res = word_reader(file)
    elif 'pdf' in file:
        res = pdf_reader(file)
    else:
        res = 'its not doc，neither nor pdf，File format is not supported！'
    return res


if __name__ == '__main__':
    path = "/Users/XXXXX/Mine/XXXXX/"
    abs_files = get_files(path)
    print(abs_files)
    for file in abs_files:
        file_text = file_reader(file)
        print(file_text)